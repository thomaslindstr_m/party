const parties = require('./index.js');

async function expectToThrow(test, callback) {
    try {
        await test();
    } catch (error) {
        callback(error);
        return;
    }

    throw new Error('No error thrown.');
}

test('should create parties', () => {
    parties('id-1');
});

test('should be able to create multiple parties with the same id', () => {
    parties('id-2');
    parties('id-2');
});

test('should not create parties with missing id', () => (
    expectToThrow(() => {
        parties();
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('id');
    })
));

test('should assert initial party existence to false', () => {
    const party = parties('id-3');
    expect(party.exists()).toBe(false);
});

test('should be able to host a party', () => {
    const party = parties('id-4');

    return party.host(new Promise((resolve) => {
        resolve();
    }));
});

test('should resolve after hosting', () => {
    const party = parties('id-5');

    return party.host(new Promise((resolve) => {
        resolve('test');
    })).then((message) => {
        expect(message).toBe('test');
    });
});

test('should reject after failed hosting', () => {
    const party = parties('id-6');

    return party.host(new Promise((resolve, reject) => {
        reject(new Error('test'));
    })).catch((error) => {
        expect(error.message).toBe('test');
    });
});

test('should fail hosting with missing promise', () => (
    expectToThrow(() => {
        const party = parties('id-7');
        return party.host();
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('promise');
    })
));

test('should exist when hosting', () => {
    const party = parties('id-9');

    party.host(new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, 1000);
    }));

    expect(party.exists()).toBe(true);
});

test('should not exist after hosting', () => {
    const party = parties('id-10');

    return party.host(new Promise((resolve) => {
        resolve();
    })).then(() => {
        expect(party.exists()).toBe(false);
    });
});

test('should be able to crash parties', () => {
    const party = parties('id-11');

    party.host(new Promise((resolve) => {
        setTimeout(() => {
            resolve('test');
        }, 1000);
    }));

    return party.crash().then((message) => {
        expect(message).toBe('test');
    });
});

test('should be able to multi-crash parties', () => {
    const party = parties('id-12');

    party.host(new Promise((resolve) => {
        setTimeout(() => {
            resolve('test');
        }, 1000);
    })).then((message) => {
        expect(message).toBe('test');
    });

    return Promise.all([
        party.crash().then((message) => {
            expect(message).toBe('test');
        }),
        party.crash().then((message) => {
            expect(message).toBe('test');
        }),
        party.crash().then((message) => {
            expect(message).toBe('test');
        })
    ]);
});

test('should fail when crash non-existent party', () => (
    expectToThrow(() => {
        const party = parties('id-13');
        return party.crash();
    }, (error) => {
        expect(error.code).toBe('unable_to_crash_party');
        expect(error.data[0]).toBe('Party not yet hosted');
        expect(error.data[1]).toBe('id-13');
    })
));
