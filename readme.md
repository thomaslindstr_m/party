# party

[![build status](https://gitlab.com/thomaslindstr_m/party/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/party/commits/master)

crash parties to avoid executing a function that expects the same result more than once. *pretty much an utility to batch equal requests together into one, to avoid doing the same operation twice*.

```
npm install @amphibian/party
```

```javascript
const parties = require('@amphibian/party');

// Assume a hypothetical asynchronous pure function
// NOTE Pure functions always return the same result given identical arguments.
function fetchPartyList(url) {
    const requestId = `fetchPartyList:${url}`;
    const party = parties(requestId);

    if (party.exists()) {
        return party.crash();
    }

    return party.host(
        fetch(`${PARTY_API_URL}/parties/list`)
            .then((response) => response.json())
            .then((response) => response.partyList)
    );
}

// Instead of calling “fetch” three times, triggering three separate network requests, this will, when utilizing @amphibian/party, only trigger one
fetchPartyList();
fetchPartyList();
fetchPartyList();
```

## `parties`

Returns a `party` `Object` provided the required `id` argument.

```javascript
const party = parties('my-unique-party-id');
```

### `party.exists`

Returns a `Boolean` describing the existence of the party. A `party` exists when it's currently being hosted.

```javascript
party.exists(); // false
```

### `party.host`

Takes `Promise` as an argument that should resolve or reject when the task is complete. When a `party` is being hosted, it may be `crash`ed.

```javascript
party.host(new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('thanks for waiting');
    }, 1000);
}));
```

### `party.crash`

Crashes the current party. In effect, `party.crash()` just returns the `host`ed `Promise`.

```javascript
party.crash().then((message) => {
    console.log(message); // > 'thanks for waiting'
});
```

## Usage with @amphibian/cache

To [hypercharge](https://media.giphy.com/media/E5TVttEnaz4ME/giphy.gif) your functions, consider utilizing [@amphibian/cache](https://www.npmjs.com/package/@amphibian/cache) in companionship with `@amphibian/party`.

```javascript
const parties = require('@amphibian/party');
const cache = require('@amphibian/cache').createCache();

function fetchPartyList(url) {
    const requestId = `fetchPartyList:${url}`;
    const cache = cache.open(requestId);

    if (cache.fresh()) {
        return Promise.resolve(cache.get());
    }

    const party = parties(requestId);

    if (party.exists()) {
        return party.crash();
    }

    return party.host(
        fetch(`${PARTY_API_URL}/parties/list`)
            .then((response) => response.json())
            .then(({partyList}) => {
                cache.set(partyList, {lifetime: 1000 * 30});
                return partyList;
            })
    );
}
```
