const objectHasProperty = require('@amphibian/object-has-property');
const errors = require('@amphibian/errors');

const parties = {};

/**
 * Party hard
 * @param {string} id
 * @returns {object} party
**/
function party(id) {
    if (!id) {
        throw errors.missingRequiredParameters(null, 'id');
    }

    let currentParty;

    if (objectHasProperty(parties, id)) {
        currentParty = parties[id];
    } else {
        currentParty = {
            party: undefined,
            exists() {
                return Boolean(this.party);
            },
            host(promise) {
                if (!promise) {
                    throw errors.missingRequiredParameters(null, 'promise');
                }

                this.party = promise.then((argument) => {
                    this.party = undefined;
                    return argument;
                }).catch((error) => {
                    this.party = undefined;
                    throw error;
                });

                return this.party;
            },
            crash() {
                if (!this.exists()) {
                    return Promise.reject(errors.invalidInput('unable_to_crash_party', 'Party not yet hosted', id));
                }

                return this.party;
            }
        };

        parties[id] = currentParty;
    }

    return currentParty;
}

module.exports = party;
